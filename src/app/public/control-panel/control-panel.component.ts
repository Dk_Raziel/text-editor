import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {

  boldClick() {
    this.handleSelection('text--bold');
  }
  italicClick() {
    this.handleSelection('text--italic');
  }
  underlineClick() {
    this.handleSelection('text--underline');
  }

  handleSelection(addClass: string) {
    const span = document.createElement('span');

    if (window.getSelection) {
      const sel = window.getSelection();
      if (sel.rangeCount) {
        const range = sel.getRangeAt(0).cloneRange();
        const parent = range.commonAncestorContainer.parentElement;
        const parentTag = parent.tagName.toLowerCase();
        if (parentTag === 'span') {
          if (parent.classList.contains(addClass)) {
            parent.classList.remove(addClass);
          } else {
            parent.classList.add(addClass);
          }
        } else {
          span.classList.add(addClass);
          range.surroundContents(span);
          sel.removeAllRanges();
          sel.addRange(range);
        }

      }
    }
  }

}
