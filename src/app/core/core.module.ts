import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SynonymsDirective } from './directives/synonyms.directive';

@NgModule({
  declarations: [
    SynonymsDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SynonymsDirective
  ]
})
export class CoreModule { }
