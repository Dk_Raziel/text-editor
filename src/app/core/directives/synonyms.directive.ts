import { Directive, HostListener, ElementRef } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Directive({
  selector: '[appSynonyms]'
})
export class SynonymsDirective {

  constructor(
    el: ElementRef,
    private http: HttpClient
    ) {  }

  @HostListener('dblclick', ['$event'])
  onDblClick(event) {
    const selection = window.getSelection().toString().trim();
    const params = new HttpParams().set('rel_syn', selection);

    this.http.get('https://api.datamuse.com/words', {params: params})
      .subscribe(
        (res) => console.log(res),
        (err) => console.error(err)
      );
  }
}
